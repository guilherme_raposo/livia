package br.com.lector.web.livia.controller;

import java.util.List;

import com.ibm.watson.assistant.v2.model.MessageResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import br.com.lector.web.livia.mapper.ApiConfigurationMapper;
import br.com.lector.web.livia.model.ApiConfiguration;
import br.com.lector.web.livia.model.QueryDTO;
import br.com.lector.web.livia.service.Instances;
import br.com.lector.web.livia.service.LivIA;

@RestController
@RequestMapping("/")
public class RootController {

	List<ApiConfiguration> apiConfigurations;

	@Autowired
	private ApiConfigurationMapper apiConfigurationMapper;

	@PostMapping("")
	public ResponseEntity<String> queryAssistant(@RequestBody QueryDTO body) {
		String text = body.getText();
		String accountId = body.getAccountId();
		String preferredLanguage = body.getPreferredLanguage();
		String sessionId = getSessionId();
		ApiConfiguration config = fetchApiConfiguration();

		LivIA livIA = Instances.getInstance(sessionId, preferredLanguage, config);

		MessageResponse messageResponse;

		try {
			if (preferredLanguage.equals(livIA.getCurrentLanguage())) {
				messageResponse = livIA.query(text, accountId);
			} else {
				throw new Exception("LANGUAGE_KEY_CHANGED");
			}
		} catch (Exception e) {
			livIA = Instances.replaceInstance(sessionId, preferredLanguage, config);
			messageResponse = livIA.query(text, accountId);
		}

		return new ResponseEntity<String>(messageResponse.toString(), HttpStatus.OK);
	}

	private ApiConfiguration fetchApiConfiguration() {
		if (apiConfigurations == null || apiConfigurations.size() == 0) {
			apiConfigurations = apiConfigurationMapper.getApiConfigurations();
		}
		return apiConfigurations.get(0);
	}

	private String getSessionId() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String sessionId = attr.getRequest().getSession().getId();

		return sessionId;
	}
}
