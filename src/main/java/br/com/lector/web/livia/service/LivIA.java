package br.com.lector.web.livia.service;

import java.util.HashMap;
import java.util.Map;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.CreateSessionOptions;
import com.ibm.watson.assistant.v2.model.MessageContext;
import com.ibm.watson.assistant.v2.model.MessageContextSkill;
import com.ibm.watson.assistant.v2.model.MessageContextSkills;
import com.ibm.watson.assistant.v2.model.MessageInput;
import com.ibm.watson.assistant.v2.model.MessageInputOptions;
import com.ibm.watson.assistant.v2.model.MessageOptions;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.SessionResponse;

import br.com.lector.web.livia.model.ApiConfiguration;
import br.com.lector.web.livia.model.AssistantConfiguration;

public class LivIA {
	private Assistant assistant;
	private String assistantId;
	private String sessionId;
	private String currentLanguage;

	public LivIA(ApiConfiguration config, String preferredLanguage) {
		Map<String, String> assistantMap = new HashMap<String, String>();
		for (AssistantConfiguration c : config.getAssistants()) {
			assistantMap.put(c.getLanguageKey(), c.getId());
		}
		currentLanguage = assistantMap.containsKey(preferredLanguage) ? preferredLanguage : "pt_BR";
		assistantId = assistantMap.get(preferredLanguage);

		IamAuthenticator authenticator = new IamAuthenticator(config.getApiKey());
		assistant = new Assistant(config.getVersion(), authenticator);
		assistant.setServiceUrl(config.getUrl());

		CreateSessionOptions options = new CreateSessionOptions.Builder(assistantId).build();

		SessionResponse response = assistant.createSession(options).execute().getResult();
		sessionId = response.getSessionId();
	}

	public String getCurrentLanguage() {
		return currentLanguage;
	}

	public MessageResponse query(String query, String accountId) {

		MessageInputOptions inputOptions = new MessageInputOptions.Builder().returnContext(true).build();

		MessageInput input = new MessageInput.Builder()
				.messageType("text")
				.text(query)
				.options(inputOptions)
				.build();

		MessageOptions options = new MessageOptions.Builder(assistantId, sessionId)
				.input(input)
				.context(getAccountContext(accountId))
				.build();

		MessageResponse response = assistant.message(options).execute().getResult();

		return response;
	}

	private MessageContext getAccountContext(String accountId) {
		Map<String, Object> userDefinedContext = new HashMap<>();

		userDefinedContext.put("account_id", accountId + "");

		MessageContextSkill mainSkillContext = new MessageContextSkill.Builder()
				.userDefined(userDefinedContext)
				.build();

		MessageContextSkills skillsContext = new MessageContextSkills();
		skillsContext.put("main skill", mainSkillContext);

		MessageContext context = new MessageContext.Builder()
				.skills(skillsContext)
				.build();

		return context;
	}
}
