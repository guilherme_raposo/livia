package br.com.lector.web.livia.model;

import javax.validation.constraints.NotNull;

public class QueryDTO {
	@NotNull(message = "text property cannot be null")
	private String text;

	@NotNull(message = "preferredLanguage property cannot be null")
	private String preferredLanguage;

	@NotNull(message = "accountId property cannot be null")
	private String accountId;

	public String getText() {
		return text;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public String getAccountId() {
		return accountId;
	}

}
