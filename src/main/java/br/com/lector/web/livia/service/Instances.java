package br.com.lector.web.livia.service;

import java.util.HashMap;
import java.util.Map;

import br.com.lector.web.livia.model.ApiConfiguration;

public class Instances {
	private static Map<String, LivIA> instances = new HashMap<String, LivIA>();

	public static LivIA getInstance(String sessionId, String preferredLanguage, ApiConfiguration configuration) {
		if (instances.containsKey(sessionId)) {
			return instances.get(sessionId);
		} else {
			LivIA livia = new LivIA(configuration, preferredLanguage);
			instances.put(sessionId, livia);
			return livia;
		}
	}

	public static LivIA replaceInstance(String sessionId, String preferredLanguage, ApiConfiguration configuration) {
		LivIA livia = new LivIA(configuration, preferredLanguage);
		instances.replace(sessionId, livia);

		return livia;
	}
}
