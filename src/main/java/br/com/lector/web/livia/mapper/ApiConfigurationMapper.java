package br.com.lector.web.livia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import br.com.lector.web.livia.model.ApiConfiguration;

@Mapper
public interface ApiConfigurationMapper {
	public List<ApiConfiguration> getApiConfigurations();
}
