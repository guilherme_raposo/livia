package br.com.lector.web.livia.model;

import java.util.List;

public class ApiConfiguration {
	private String apiKey;
	private String url;
	private String version;
	private List<AssistantConfiguration> assistants;

	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<AssistantConfiguration> getAssistants() {
		return assistants;
	}

	public void setAssistants(List<AssistantConfiguration> assistants) {
		this.assistants = assistants;
	}

}
