package br.com.lector.web.livia.model;

public class AssistantConfiguration {
	private String id;
	private String languageKey;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLanguageKey() {
		return this.languageKey;
	}

	public void setLanguageKey(String languageKey) {
		this.languageKey = languageKey;
	}

}
