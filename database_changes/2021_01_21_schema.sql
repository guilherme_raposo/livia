CREATE DATABASE `livia`;

USE `livia`;

CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `version` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `assistant` (
  `id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `api_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi9ctkmaeng2x8ra9s2li644f7` (`api_id`),
  CONSTRAINT `assistant_FK` FOREIGN KEY (`api_id`) REFERENCES `api` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `api` VALUES (1,'yyysUt2Nm8hg1quOeyzX5Aktc87UcJLsQYYDNKiGCfd3','https://api.us-south.assistant.watson.cloud.ibm.com','2020-09-24');
INSERT INTO `assistant` VALUES ('7353ef89-f800-436f-b098-2eca9c1144b1','en_US',1),('a3c5ec0c-e05d-4ffa-9c2f-a27d38af4e2b','pt_BR',1);